/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizacion;

public class Main {
    public static void main(String[] args) {
        
        Cotizacion numdecoti = new Cotizacion();
        
        numdecoti.setNumdecoti(0123);
        numdecoti.setPrecio(220000f);
        numdecoti.setPorcentpagoini(0.25f);
        numdecoti.setPlazo(36);
        
        float pagoInicial = numdecoti.calcularPagoini();
        System.out.println("Pago inicial: " + pagoInicial);
        
        float totalFin = numdecoti.calcularPagofin();
        System.out.println("Total a financiar: " + totalFin);
        
        float pagoMen = numdecoti.calcularPagomen();
        System.out.println("Pago mensual: " + pagoMen);
        
        Cotizacion coti = new Cotizacion(0123, 220000f, 0.25f, 36);
        
        pagoInicial = coti.calcularPagoini();
        System.out.println("Pago inicial: " + pagoInicial);
        
        totalFin = coti.calcularPagofin();
        System.out.println("Total a financiar: " + totalFin);
        
        pagoMen = coti.calcularPagomen();
        System.out.println("Pago mensual: " + pagoMen);
        
        Cotizacion cot = new Cotizacion(numdecoti);
        
        pagoInicial = cot.calcularPagoini();
        System.out.println("Pago inicial: " + pagoInicial);
        
        totalFin = cot.calcularPagofin();
        System.out.println("Total a financiar: " + totalFin);
        
        pagoMen = cot.calcularPagomen();
        System.out.println("Pago mensual: " + pagoMen);
                        
    }
    
}

