/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizacion;

public class Cotizacion {
    private int numdecoti;
    private String descripcion;
    private float precio;
    private float porcentpagoini;
    private int plazo;
    
    public Cotizacion(){
        this.numdecoti = 0;
        this.precio = 0;
        this.porcentpagoini = 0;
        this.plazo = 0;
    }
    
    public Cotizacion(int numdecoti, float precio, float porcentpagoini, int plazo){
        this.numdecoti = numdecoti;
        this.precio = precio;
        this.porcentpagoini = porcentpagoini;
        this.plazo = plazo;
    }
    
    public Cotizacion (Cotizacion x){
        this.numdecoti = x.numdecoti;
        this.precio = x.precio;
        this.porcentpagoini = x.porcentpagoini;
        this.plazo = x.plazo;
    }
    
    public void setNumdecoti(int numdecoti){
        this.numdecoti = numdecoti;
    }
    
    public int getNumdecoti(){
        return this.numdecoti;
    }
    
    public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public float getPrecio(){
        return this.precio;
    }
    
    public void setPorcentpagoini(float porcentpagoini){
        this.porcentpagoini = porcentpagoini;
    }
    
    public float getPorcentpagoini(){
        return this.porcentpagoini;
    }
    
    public void setPlazo(int plazo){
        this.plazo = plazo;
    }
    
    public int getPlazo(){
        return this.plazo;
    }
    
    public float calcularPagoini(){
        float pagoIni = 0.0f;
        pagoIni = this.precio*this.porcentpagoini;
        return pagoIni;
    }
    
    public float calcularPagofin(){
        float totalFin = 0.0f;
        totalFin = this.precio-this.calcularPagoini();
        return totalFin;
    }
    
    public float calcularPagomen(){
        float pagoMen = 0.0f;
        pagoMen = this.calcularPagofin()/this.plazo;
        return pagoMen;
    }
    
    
}